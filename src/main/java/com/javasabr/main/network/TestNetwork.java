package com.javasabr.main.network;

import com.javasabr.main.network.client.TestClientNetwork;
import com.javasabr.main.network.server.TestServerNetwork;

/**
 * @author Ronn
 */
public class TestNetwork {

	public static void main(String[] args) {

		TestServerNetwork serverNetwork = new TestServerNetwork();
		serverNetwork.createNetwork();

		TestClientNetwork clientNetwork = new TestClientNetwork();
		clientNetwork.createNetwork();

		try {
			Thread.sleep(1000);
		} catch(InterruptedException e) {
			e.printStackTrace();
		}

		clientNetwork.getServer().close();
	}
}
