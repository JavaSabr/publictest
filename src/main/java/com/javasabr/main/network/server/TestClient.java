package com.javasabr.main.network.server;

import com.javasabr.main.network.server.packet.MessageServerPacket;
import com.ss.rlib.network.NetworkCrypt;
import com.ss.rlib.network.packet.ReadablePacket;
import com.ss.rlib.network.server.client.impl.AbstractClient;
import org.jetbrains.annotations.NotNull;

/**
 * Реализация клиента для тестового сервера.
 * 
 * @author Ronn
 */
public class TestClient extends AbstractClient {

	public TestClient(TestConnection connection, NetworkCrypt crypt) {
		super(connection, crypt);
	}

	@Override
	public void close() {
		super.close();

		System.out.println("client closed!");
	}

    @Override
    protected void execute(@NotNull final ReadablePacket packet) {
    }

    @Override
	public void successfulConnection() {
		sendPacket(new MessageServerPacket("Hello new client!"));
	}
}
