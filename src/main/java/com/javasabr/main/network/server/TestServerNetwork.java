package com.javasabr.main.network.server;

import com.javasabr.main.network.server.packet.ServerPacket;
import com.ss.rlib.network.NetworkConfig;
import com.ss.rlib.network.NetworkFactory;
import com.ss.rlib.network.server.AcceptHandler;
import com.ss.rlib.network.server.ServerNetwork;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.StandardSocketOptions;
import java.nio.channels.AsynchronousSocketChannel;

/**
 * Реализация теста серверной сети.
 * 
 * @author Ronn
 */
public class TestServerNetwork {

	private static final NetworkConfig CONFIG = new NetworkConfig() {

		@Override
		public int getGroupSize() {
			return 3;
		}
	};

	private final AcceptHandler acceptHandler = new AcceptHandler() {

		@Override
		protected void onFailed(Throwable throwable) {
			throwable.printStackTrace();
		}

		@Override
		protected void onAccept(AsynchronousSocketChannel channel) {

			try {
				channel.setOption(StandardSocketOptions.SO_SNDBUF, 12000);
				channel.setOption(StandardSocketOptions.SO_RCVBUF, 24000);
			} catch(final IOException e) {
				LOGGER.warning(e.getMessage(), e);
			}

			final TestConnection connect = new TestConnection(network, channel, ServerPacket.class);
			final TestClient client = new TestClient(connect, null);

			connect.setOwner(client);
			client.successfulConnection();
			connect.startRead();
		}
	};

	private ServerNetwork network;

	public void createNetwork() {

		network = NetworkFactory.newDefaultAsynchronousServerNetwork(CONFIG, acceptHandler);

		try {
			network.bind(new InetSocketAddress(1500));
		} catch(IOException e) {
			throw new RuntimeException(e);
		}
	}
}
