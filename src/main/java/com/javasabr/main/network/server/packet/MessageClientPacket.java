package com.javasabr.main.network.server.packet;

import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;

/**
 * Реализация клиентского пакета с сообщением.
 * 
 * @author Ronn
 */
public class MessageClientPacket extends ClientPacket {

	private String message;

    @Override
    protected void readImpl(final @NotNull ByteBuffer buffer) {
        super.readImpl(buffer);
        message = readString(buffer);
    }

	@Override
	public void run() {
		System.out.println("Сервер получил сообщение:" + message);
	}
}
