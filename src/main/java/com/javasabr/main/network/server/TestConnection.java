package com.javasabr.main.network.server;

import com.javasabr.main.network.server.packet.MessageClientPacket;
import com.javasabr.main.network.server.packet.ServerPacket;
import com.ss.rlib.network.server.ServerNetwork;
import com.ss.rlib.network.server.client.impl.AbstractClientConnection;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;

/**
 * Реализация подключения клиента к серверу для теста.
 * 
 * @author Ronn
 */
public class TestConnection extends AbstractClientConnection {

	public TestConnection(ServerNetwork network, AsynchronousSocketChannel channel, Class<ServerPacket> sendableType) {
		super(network, channel, sendableType);
	}

	@Override
	protected void readPacket(ByteBuffer buffer) {

		final int packetSize = buffer.getShort();

		System.out.println("packet size " + packetSize);

		MessageClientPacket clientPacket = new MessageClientPacket();

		final TestClient client = (TestClient) getOwner();
		client.readPacket(clientPacket, buffer);
	}
}
