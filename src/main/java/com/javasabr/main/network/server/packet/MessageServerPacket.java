package com.javasabr.main.network.server.packet;

import java.nio.ByteBuffer;

/**
 * Реализация серверного пакета по отправке сообщения.
 * 
 * @author Ronn
 */
public class MessageServerPacket extends ServerPacket {

	private final String message;

	public MessageServerPacket(String message) {
		this.message = message;
	}

	@Override
	protected void writeImpl(ByteBuffer buffer) {
		writeInt(buffer, message.length());
		writeString(buffer, message);
	}
}
