package com.javasabr.main.network.server.packet;

import com.ss.rlib.network.packet.impl.AbstractReadablePacket;
import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;

/**
 * Реализация клиентского пакета для теста серверной сети.
 * 
 * @author Ronn
 */
public class ClientPacket extends AbstractReadablePacket implements Runnable {

    @Override
    protected void readImpl(@NotNull final ByteBuffer buffer) {
        super.readImpl(buffer);
    }

    @Override
	public void run() {
		// TODO Автоматически созданная заглушка метода

	}
}
