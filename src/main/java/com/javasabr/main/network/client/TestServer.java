package com.javasabr.main.network.client;

import com.ss.rlib.network.NetworkCrypt;
import com.ss.rlib.network.client.server.impl.AbstractServer;
import com.ss.rlib.network.packet.ReadablePacket;
import org.jetbrains.annotations.NotNull;

/**
 * @author Ronn
 */
public class TestServer extends AbstractServer {

	protected TestServer(TestConnection connection, NetworkCrypt crypt) {
		super(connection, crypt);
	}


    @Override
    protected void execute(@NotNull final ReadablePacket packet) {
    }
}
