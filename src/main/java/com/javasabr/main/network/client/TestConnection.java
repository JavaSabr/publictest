package com.javasabr.main.network.client;

import com.javasabr.main.network.client.packet.ClientPacket;
import com.javasabr.main.network.client.packet.MessageServerPacket;
import com.ss.rlib.network.client.ClientNetwork;
import com.ss.rlib.network.client.server.impl.AbstractServerConnection;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;

/**
 * @author Ronn
 */
public class TestConnection extends AbstractServerConnection {

	public TestConnection(ClientNetwork network, AsynchronousSocketChannel channel, Class<ClientPacket> sendableType) {
		super(network, channel, sendableType);
	}

	@Override
	protected void readPacket(ByteBuffer buffer) {

		final int packetSize = buffer.getShort();

		System.out.println("packet size " + packetSize);

		MessageServerPacket clientPacket = new MessageServerPacket();

		final TestServer server = (TestServer) getOwner();
		server.readPacket(clientPacket, buffer);
	}
}
