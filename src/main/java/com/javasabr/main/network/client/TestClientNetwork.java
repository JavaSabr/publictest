package com.javasabr.main.network.client;

import com.javasabr.main.network.client.packet.ClientPacket;
import com.javasabr.main.network.client.packet.MessageClientPacket;
import com.ss.rlib.network.NetworkConfig;
import com.ss.rlib.network.NetworkFactory;
import com.ss.rlib.network.client.ClientNetwork;
import com.ss.rlib.network.client.ConnectHandler;

import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousSocketChannel;

/**
 * Реализация теста клиентской сети.
 * 
 * @author Ronn
 */
public class TestClientNetwork {

	private static final NetworkConfig CONFIG = new NetworkConfig() {

		@Override
		public int getGroupSize() {
			return 3;
		}
	};

	private final ConnectHandler connectHandler = new ConnectHandler() {

		@Override
		public void onFailed(Throwable exc) {
		};

		@Override
		public void onConnect(AsynchronousSocketChannel channel) {

			final TestConnection connect = new TestConnection(network, channel, ClientPacket.class);
			server = new TestServer(connect, null);

			connect.setOwner(server);
			connect.startRead();

			server.sendPacket(new MessageClientPacket("Hello server!"));
		}
	};

	private ClientNetwork network;

	private TestServer server;

	public void createNetwork() {
		network = NetworkFactory.newDefaultAsynchronousClientNetwork(CONFIG, connectHandler);
		network.connect(new InetSocketAddress(1500));
	}

	public TestServer getServer() {
		return server;
	}
}
