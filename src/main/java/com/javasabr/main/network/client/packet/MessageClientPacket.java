package com.javasabr.main.network.client.packet;

import java.nio.ByteBuffer;

/**
 * @author Ronn
 */
public class MessageClientPacket extends ClientPacket {

	private final String message;

	public MessageClientPacket(String message) {
		this.message = message;
	}

	@Override
	protected void writeImpl(ByteBuffer buffer) {
		writeInt(buffer, message.length());
		writeString(buffer, message);
	}
}
