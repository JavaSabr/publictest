package com.javasabr.main.network.client.packet;

import com.ss.rlib.network.packet.impl.AbstractSendablePacket;
import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;

/**
 * @author Ronn
 */
public class ClientPacket extends AbstractSendablePacket {

    @Override
    public void prepareWritePosition(@NotNull final ByteBuffer buffer) {
        buffer.position(2);
    }

    @Override
    public void writePacketSize(@NotNull final ByteBuffer buffer, final int packetSize) {
        buffer.putShort(0, (short) packetSize);
    }
}
