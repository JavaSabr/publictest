package com.javasabr.main.network.client.packet;

import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;

/**
 * @author Ronn
 */
public class MessageServerPacket extends ServerPacket {

	private String message;

    @Override
    protected void readImpl(final @NotNull ByteBuffer buffer) {
        super.readImpl(buffer);
        message = readString(buffer);
    }

	@Override
	public void run() {
		System.out.println("Клиент получил сообщение:" + message);
	}
}
