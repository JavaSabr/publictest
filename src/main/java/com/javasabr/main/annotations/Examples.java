package com.javasabr.main.annotations;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;

import static java.lang.System.out;

/**
 * Created by ronn on 13.09.16.
 */
public class Examples {

    public static void main(String[] args) throws SQLException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        call4();
    }

    public static void call1() {
        foo1(null);
    }

    public static void foo1(@NotNull final Integer value) {
        out.println("value = " + value.toString());
    }

    public static void call2() {
        foo2(null);
    }

    public static void foo2(@Nullable final Integer value) {
        out.println("value = " + value.toString());
    }

    public void call3() throws NoSuchMethodException {

        final Method foo3 = getClass().getMethod("foo3");
        final Deprecated annotation = foo3.getAnnotation(Deprecated.class);

        out.println("is the method deprecated? " + (annotation != null));
    }

    @Deprecated
    public void foo3() {
        out.println("this method is deprecated.");
    }

    public static void call4() {
        final Class<FunctionalInterface> check = FunctionalInterface.class;
        out.println("is the Runnable FI? " + (Runnable.class.getAnnotation(check) != null));
        out.println("is the Inter1 FI? " + (Inter1.class.getAnnotation(check) != null));
        out.println("is the Inter2 FI? " + (Inter2.class.getAnnotation(check) != null));
    }

    interface Inter1 extends Runnable {
    }

    @FunctionalInterface
    interface Inter2 extends Inter1 {
    }
}
