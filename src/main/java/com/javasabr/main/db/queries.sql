SELECT
  d.name AS developer,
  c.name AS company
FROM developer d, company c
WHERE 0 < (SELECT count(*)
           FROM developer_position dp
           WHERE dp.developer_id = d.id AND 0 <
                                            (SELECT count(*)
                                             FROM position p
                                             WHERE p.id = dp.position_id AND p.name = 'Senior'));

SELECT
  count(d.name)                 AS developers,
  (SELECT c.name
   FROM company c
   WHERE c.id = d.company_id)   AS company,
  (SELECT p.name
   FROM position p
   WHERE p.id = dp.position_id) AS position
FROM (SELECT *
      FROM developer d
      WHERE d.name LIKE ('%A%')) AS d
  LEFT JOIN developer_position dp ON dp.developer_id = d.id
GROUP BY d.company_id, dp.position_id;

SELECT
  c.name,
  p.name,
  count(d.id)          AS count,
  (SELECT count(*)
   FROM developer sd
     LEFT JOIN developer_position sdp ON sdp.developer_id = sd.id
     LEFT JOIN position sp ON sp.id = sdp.position_id
   WHERE sp.id = p.id) AS total
FROM company c
  LEFT JOIN developer d ON d.company_id = c.id
  LEFT JOIN developer_position dp ON dp.developer_id = d.id
  LEFT JOIN position p ON p.id = dp.position_id
GROUP BY c.name, p.id
ORDER BY count;

SELECT
  d.name,
  c.name
FROM developer d
  LEFT JOIN company c ON c.id = d.company_id
ORDER BY d.name;

SELECT
  d.name,
  c.name
FROM developer d
  JOIN company c ON c.id = d.company_id
ORDER BY d.name;

SELECT
  d.name,
  c.name
FROM developer d
  RIGHT JOIN company c ON c.id = d.company_id
ORDER BY d.name;