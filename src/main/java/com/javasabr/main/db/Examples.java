package com.javasabr.main.db;

import static java.lang.System.out;
import com.mysql.jdbc.Driver;

import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.Properties;

/**
 * Created by ronn on 13.09.16.
 */
public class Examples {

    public static void main(String[] args) throws SQLException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        connect();
    }

    public static void connect() throws SQLException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        Driver.getPlatform();
        //Class.forName("com.mysql.fabric.jdbc.FabricMySQLDriver").getConstructor().newInstance();

        final Properties info = new Properties();
        info.put("user", "root");
        info.put("password", "root");

        final Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/puzzlers", info);

        queries2(connection);
    }

    public static void queries(final Connection connection) throws SQLException {

        final Statement statement = connection.createStatement();
        final ResultSet resultSet = statement.executeQuery("SELECT * FROM developers");
        final String name = resultSet.getString(1);
    }

    public static void queries2(final Connection connection) throws SQLException {

        final PreparedStatement statement = connection.prepareStatement("SELECT d.id FROM developers d " +
                " LEFT JOIN company c ON c.id = d.company_id " +
                " LEFT JOIN developer_position dp ON dp.developer_id = d.id " +
                " LEFT JOIN position p ON p.id = dp.position_id " +
                " WHERE d.name LIKE ? AND c.id > ? AND p.name LIKE ? ");

        statement.setString(1, "%A%");
        statement.setInt(2, 0);

        final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            final int id = resultSet.getInt(1);
            out.print(id);
        }
    }
}
