package com.javasabr.main.memory;

/**
 * Created by ronn on 15.08.16.
 */
public class HBExample3<T> {

    static volatile int BARRIER; int sink;

    private T val;

    public synchronized void setVal(final T val) {
        if(this.val == null) this.val = val;
    }

    public T getVal() {
        sink = BARRIER;
        return val;
    }
}
