package com.javasabr.main.memory;

import com.ss.rlib.util.dictionary.DictionaryFactory;
import com.ss.rlib.util.dictionary.IntegerDictionary;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntObjectHashMap;
import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10)
@Fork(value = 1)
@Measurement(iterations = 5)
public class BestPractices1 {

    public void autoboxing() {

        int a = 1;
        Integer b = a; b = Integer.valueOf(a);
        int c = b; c = b.intValue();

        // default
        List<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.add(2);

        Map<Integer, String> integerToString = new HashMap<>();
        integerToString.put(1, "one");
        integerToString.put(2, "two");

        // Trove
        TIntArrayList ints = new TIntArrayList();
        ints.add(1);
        ints.add(2);

        TIntObjectHashMap intToString = new TIntObjectHashMap();
        integerToString.put(1, "one");
        integerToString.put(2, "two");

        // my implementation
        IntegerDictionary<String> dictionary = DictionaryFactory.newIntegerDictionary();
        dictionary.put(1, "one");
        dictionary.put(2, "two");
    }

    public void stringConcat() {

        String var1 = "part1" + "part2";

        System.out.print(var1);

        String var2 = "part1";
        var2 += "part2";
        var2 += "part3" + "part4";

        System.out.print(var2);

        String var3 = "part1";

        for(int i = 2, length = 10; i < length; i++) {
            var3 += "part" + i;
        }
    }

    public void stringBuilder() {

        StringBuilder b1 = new StringBuilder("part1").append("part2");
        String var1 = b1.toString();

        System.out.print(var1);

        StringBuilder b2 = new StringBuilder("part1");
        b2.append("part2");
        b2.append("part3").append("part4");

        String var2 = b2.toString();

        System.out.print(var2);

        StringBuilder b3 = new StringBuilder("part1");

        for(int i = 2, length = 10; i < length; i++) {
            b3.append("part").append(i);
        }

        String var3 = b3.toString();
    }

    public void method1(int arg1, Object... args) {
        final Object[] objects = args;
        for (Object object : objects) {
            System.out.print(object);
        }
    }

    public void useMethod1() {
        method1(111, "Efewwfwef", new Object(), "dgrer", 5);
    }
}
