package com.javasabr.main.memory;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ronn on 15.06.16.
 */
public class UsingThreadLocal {

    private static final ThreadLocal<DateFormat> LOCAL_DF = ThreadLocal.withInitial(DateFormat::getInstance);
    private static final ThreadLocal<List<Object>> LOCAL_LIST = ThreadLocal.withInitial(ArrayList::new);

    private static final DateFormat DATE_FORMAT = DateFormat.getInstance();
    private static final List<Object> LIST = new ArrayList<>();

    public static void example() {

        final DateFormat dateFormat = LOCAL_DF.get();
        dateFormat.format(new Date());

        final List<Object> list = LOCAL_LIST.get();
        list.clear();
        // do something

        synchronized (DATE_FORMAT) {
            DATE_FORMAT.format(new Date());
        }

        synchronized (LIST) {
            LIST.clear();
            // do something
        }
    }
}
