package com.javasabr.main.memory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ronn on 16.06.16.
 */
public class ReturnAnonymousClass {

    private class InnerClass {
        private int val1;
    }

    public Map<String, String> getProperties() {
        return new HashMap<String, String>() {{
            put("key1", "val1");
            put("key2", "val2");
            put("key3", "val3");
            foo();
        }};
    }

    public InnerClass getObject() {
        return new InnerClass();
    }

    public void foo() {
    }
}
