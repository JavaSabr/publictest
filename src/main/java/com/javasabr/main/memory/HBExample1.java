package com.javasabr.main.memory;

/**
 * Created by ronn on 15.08.16.
 */
public class HBExample1<T> {

    private T val;

    public synchronized void setVal(final T val) {
        if(this.val == null) this.val = val;
    }

    public synchronized T getVal() {
        return val;
    }
}
