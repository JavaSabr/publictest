package com.javasabr.main.memory;

/**
 * Created by ronn on 15.08.16.
 */
public class HBExample2<T> {

    private T val;

    public synchronized void setVal(final T val) {
        if(this.val == null) this.val = val;
    }

    public T getVal() {
        return val;
    }
}
