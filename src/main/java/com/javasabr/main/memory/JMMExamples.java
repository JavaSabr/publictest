package com.javasabr.main.memory;

/**
 * Created by ronn on 11.08.16.
 */
public class JMMExamples {

    public static <T> void f() {

        /*
        {
            int x = 10;
            int b = x;
            int c = b + x;

            x = c - b;
            c = b;

            long value = 0;

            value = 0;

            System.out.println(value);
            // 0, -1,
            // 0xFFFFFFFF00000000 | 18446744069414584000

            int[] array = new int[4]; array[0] = 1; array[1] = 1;

            array[0] = 2;
            array[1] = 2;

            int a1 = array[0];
            int a2 = array[1];

            System.out.println(a1 == a2);

            BitSet bitSet = new BitSet();

            bitSet.set(0);
            bitSet.set(1);

            boolean b1 = bitSet.get(0);
            boolean b2 = bitSet.get(1);

            System.out.println(b1 == b2);
        }

        {
            int a = 0; int b = 0;
            a = 1;
            b = 2;
            System.out.println(b);
            System.out.println(a);
        }

        {
            int x = 0; int y = 0;

            int v1 = x;
            int v2 = y;

            y = 2;
            x = 1;
        }

        {
            volatile int x = 0; volatile int y = 0;
            x = 10;
            y = 20;

            System.out.println(x);
            System.out.println(y);
        }

        {
            int x = 0; volatile int y = 0;
            x = 1;
            y = 1;

            int v1 = y;
            int v2 = x;
        }

        {
            volatile int a = 0; int b, c, d, e;

            b = 1;
            c = 2;
            a = 5;
            d = 3;
            e = 4;

            int v1 = b;
            int v2 = c;
            int v3 = d;
            int v4 = a;
            int v5 = e;
        }

        {
            volatile int a = 0, b; int c, d, e, g;

            c = 1;
            d = 2;
            a = 3;
            int v1 = b;
            int v2 = e;
            int v3 = g;

            e = 4;
            g = 5;
            b = 6;

            int v1 = a;
            int v2 = c;
            int v3 = d;
        }*/
    }
}
