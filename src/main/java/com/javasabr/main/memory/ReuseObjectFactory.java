package com.javasabr.main.memory;

import com.ss.rlib.util.pools.Pool;
import com.ss.rlib.util.pools.PoolFactory;

/**
 * Created by ronn on 15.06.16.
 */
public class ReuseObjectFactory {

    private static final Pool<ReuseObject> POOL = PoolFactory.newPool(ReuseObject.class);

    public static ReuseObject create() {
        final ReuseObject object = POOL.take();
        if(object == null) return new ReuseObject();
        return object;
    }

    private static class ReuseObject {

        private int val1;
        private int val2;
        private int val3;

        public void release() {
            POOL.put(this);
        }
    }

    public static void example() {
        final ReuseObject object1 = ReuseObjectFactory.create();
        final ReuseObject object2 = ReuseObjectFactory.create();
        // do something
        object1.release();
        object2.release();
    }
}
