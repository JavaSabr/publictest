package com.javasabr.main.array;

import com.ss.rlib.util.array.Array;
import com.ss.rlib.util.array.ArrayFactory;
import com.ss.rlib.util.array.UnsafeArray;
import com.ss.rlib.util.array.impl.FastArray;
import org.openjdk.jmh.annotations.Benchmark;

import java.util.ArrayList;
import java.util.List;

/**
 * Реализация тестов на производительность для сравнение {@link ArrayList} с
 * {@link FastArray}.
 * 
 * @author Ronn
 */
public class PerformanceTestArray {

	public static final int COLLECTION_SIZE = 10000000;
	public static final int REMOVE_COUNT = 10000;
	public static final int ITERATION_SIZE = 10;

	private static final List<Integer> jdkArrayList = new ArrayList<>();

	private static final Array<Integer> myArray = ArrayFactory.newArray(Integer.class);

	static {

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
			jdkArrayList.add(i);
		}

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
			myArray.add(i);
		}
	}

	@Benchmark
	public Object jdkArrayListAdd() {

		final List<Integer> collection = new ArrayList<>();

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
			collection.add(i);
		}

		return collection;
	}

	@Benchmark
	public Object jdkArrayListAddAll() {

		final List<Integer> addedCollection = jdkArrayList;
		final List<Integer> collection = new ArrayList<>();

		for(int i = 0, length = ITERATION_SIZE; i < length; i++) {
			collection.addAll(addedCollection);
		}

		return collection;
	}

	@Benchmark
	public Object jdkArrayListRemove() {

		final List<Integer> collection = new ArrayList<>(jdkArrayList);

		for(int i = 0, length = REMOVE_COUNT; i < length; i++) {
			collection.remove(0);
		}

		return collection;
	}

	@Benchmark
	public Object myArrayListAdd() {

		final Array<Integer> collection = ArrayFactory.newArray(Integer.class);
        final UnsafeArray<Integer> unsafe = collection.asUnsafe();
        unsafe.prepareForSize(COLLECTION_SIZE);

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
            unsafe.unsafeAdd(i);
		}

		return collection;
	}

	@Benchmark
	public Object myArrayListAddAll() {

		final Array<Integer> addedCollection = myArray;
		final Array<Integer> collection = ArrayFactory.newArray(Integer.class);
        final UnsafeArray<Integer> unsafe = collection.asUnsafe();
        unsafe.prepareForSize(COLLECTION_SIZE * ITERATION_SIZE);

		for(int i = 0, length = ITERATION_SIZE; i < length; i++) {
			collection.addAll(addedCollection);
		}

		return collection;
	}

	@Benchmark
	public Object myArrayListRemove() {

		final Array<Integer> collection = ArrayFactory.newArray(Integer.class);
        final UnsafeArray<Integer> unsafe = collection.asUnsafe();
        unsafe.prepareForSize(COLLECTION_SIZE);
		collection.addAll(myArray);

		for(int i = 0, length = REMOVE_COUNT; i < length; i++) {
			collection.fastRemove(0);
		}

		return collection;
	}
}
