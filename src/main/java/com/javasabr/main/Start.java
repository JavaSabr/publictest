package com.javasabr.main;

import com.javasabr.main.reuse.GlobalThreadLocalPoolPerfomanceTest;

import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

/**
 * @author Ronn
 */
public class Start {

	public static void main(final String[] args) throws RunnerException {

		final OptionsBuilder builder = new OptionsBuilder();
		// builder.include(PerfomanceTestIteration.class.getName());
		// builder.include(PerfomanceTestArray.class.getName());
		// builder.include(ThreadLocalPerfomanceTest.class.getName());
		// builder.include(LocalObjectsPerfomanceTest.class.getName());
		// builder.include(GlobalThreadUnsafePoolPerfomanceTest.class.getName());
		// builder.include(GlobalThreadSafePoolPerfomanceTest.class.getName());
		builder.include(GlobalThreadLocalPoolPerfomanceTest.class.getName());
		builder.mode(Mode.SingleShotTime);
		builder.warmupIterations(10);
		builder.measurementIterations(2);
		builder.jvmArgs("-server");
		builder.forks(1);

		final Options opts = builder.build();

		final Runner runner = new Runner(opts);
		runner.run();
	}
}
