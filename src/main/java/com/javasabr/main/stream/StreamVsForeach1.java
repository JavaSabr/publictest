package com.javasabr.main.stream;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10)
@Fork(value = 1)
@Measurement(iterations = 5)
public class StreamVsForeach1 {

    private static final String[] EXAMPLES = {"Alexander", "Yury", "Dima", "Ruslan", "Oleg", "Konstantin", "Alexey", "Nikita", "Roma", "Andry", "Ivan", "Sergey"};

    public static final List<String> STRINGS_SMALL = new ArrayList<>();
    public static final List<String> STRINGS_BIG = new ArrayList<>();

    static {

        final ThreadLocalRandom current = ThreadLocalRandom.current();

        for(int i = 0, length = 3; i < length; i++) {
            STRINGS_SMALL.add(EXAMPLES[current.nextInt(0, EXAMPLES.length - 1)]);
        }

        for(int i = 0, length = 10000; i < length; i++) {
            STRINGS_BIG.add(EXAMPLES[current.nextInt(0, EXAMPLES.length - 1)]);
        }
    }

    @Benchmark
    public List<String> foreach_small() {

        List<String> result = new ArrayList<>(STRINGS_SMALL.size());

        for (String string : STRINGS_SMALL) {
            result.add(string);
        }

        return result;
    }

    @Benchmark
    public List<String> foreach_big() {

        List<String> result = new ArrayList<>(STRINGS_BIG.size());

        for (String string : STRINGS_BIG) {
            result.add(string);
        }

        return result;
    }

    @Benchmark
    public List<String> stream_small() {

        List<String> result = new ArrayList<>(STRINGS_SMALL.size());

        STRINGS_SMALL.stream().forEach(result::add);

        return result;
    }

    @Benchmark
    public List<String> stream_big() {

        List<String> result = new ArrayList<>(STRINGS_BIG.size());

        STRINGS_BIG.stream().forEach(result::add);

        return result;
    }
}
