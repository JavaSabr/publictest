package com.javasabr.main.stream;

import com.ss.rlib.geom.Quaternion4f;
import com.ss.rlib.geom.Vector3f;

/**
 * Created by ronn on 13.04.16.
 */
public class GameObject {

    private Vector3f location;

    private Quaternion4f rotation;

    private int objectId;

    public GameObject(Vector3f location, Quaternion4f rotation, int objectId) {
        this.location = location;
        this.rotation = rotation;
        this.objectId = objectId;
    }

    public int getObjectId() {
        return objectId;
    }

    public Quaternion4f getRotation() {
        return rotation;
    }

    public Vector3f getLocation() {
        return location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameObject that = (GameObject) o;

        return objectId == that.objectId;

    }

    @Override
    public int hashCode() {
        return objectId;
    }
}
