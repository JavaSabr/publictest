package com.javasabr.main.stream;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10)
@Fork(value = 1)
@Measurement(iterations = 5)
public class StreamVsForeach2 {

    private static final String[] EXAMPLES = {"Alexander", "Yury", "Dima", "Ruslan", "Oleg", "Konstantin", "Alexey", "Nikita", "Roma", "Andry", "Ivan", "Sergey"};

    public static final List<String> STRINGS_SMALL = new ArrayList<>();
    public static final List<String> STRINGS_BIG = new ArrayList<>();

    static {

        final ThreadLocalRandom current = ThreadLocalRandom.current();

        for(int i = 0, length = EXAMPLES.length; i < length; i++) {
            STRINGS_SMALL.add(EXAMPLES[i]);
        }

        for(int i = 0, length = 10000; i < length; i++) {
            STRINGS_BIG.add(EXAMPLES[current.nextInt(0, EXAMPLES.length - 1)]);
        }
    }

    @Benchmark
    public List<String> classic_small() {

        List<String> toSort = new ArrayList<>();

        for (String string : STRINGS_SMALL) {

            if(!string.startsWith("A")) {
                continue;
            } else if(!string.endsWith("y")) {
                continue;
            }

            toSort.add(string);
        }

        Collections.sort(toSort, String::compareToIgnoreCase);

        return toSort;
    }

    @Benchmark
    public List<String> classic_big() {

        List<String> toSort = new ArrayList<>();

        for (String string : STRINGS_BIG) {

            if(!string.startsWith("A")) {
                continue;
            } else if(!string.endsWith("y")) {
                continue;
            }

            toSort.add(string);
        }

        Collections.sort(toSort, String::compareToIgnoreCase);

        return toSort;
    }

    @Benchmark
    public List<String> stream_small() {
        return STRINGS_SMALL.stream()
                .filter(string -> string.startsWith("A") && string.endsWith("y"))
                .sorted(String::compareToIgnoreCase).collect(Collectors.toList());
    }

    @Benchmark
    public List<String> stream_big() {
        return STRINGS_BIG.stream()
                .filter(string -> string.startsWith("A") && string.endsWith("y"))
                .sorted(String::compareToIgnoreCase).collect(Collectors.toList());
    }

    @Benchmark
    public List<String> stream_big_parallel() {
        return STRINGS_BIG.parallelStream().unordered()
                .filter(string -> string.startsWith("A") && string.endsWith("y"))
                .sorted(String::compareToIgnoreCase).collect(Collectors.toList());
    }
}
