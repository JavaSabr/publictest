package com.javasabr.main.stream;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.openjdk.jmh.annotations.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 3)
@Fork(value = 1)
@Measurement(iterations = 3)
public class StreamVsForeach4 {

    private static final String[] EXAMPLES = {"Alexander", "Yury", "Dima", "Ruslan", "Oleg", "Konstantin", "Alexey", "Nikita", "Roma", "Andry", "Ivan", "Sergey"};

    public static final List<String> STRINGS_SMALL = new ArrayList<>();
    public static final List<String> STRINGS_BIG = new ArrayList<>();

    static {

        final ThreadLocalRandom current = ThreadLocalRandom.current();

        for(int i = 0, length = 2; i < length; i++) {
            STRINGS_SMALL.add(EXAMPLES[current.nextInt(0, EXAMPLES.length - 1)]);
        }

        for(int i = 0, length = 8; i < length; i++) {
            STRINGS_BIG.add(EXAMPLES[current.nextInt(0, EXAMPLES.length - 1)]);
        }
    }

    @Benchmark
    public List<String> foreach_small() {

        List<String> result = new ArrayList<>(STRINGS_SMALL.size());

        for (String name : STRINGS_SMALL) {

            try(CloseableHttpClient httpClient = HttpClients.createDefault()) {
                final CloseableHttpResponse response = httpClient.execute(new HttpGet("https://httpbin.org/get?name=" + name));
                result.add(IOUtils.toString(response.getEntity().getContent()));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        return result;
    }

    @Benchmark
    public List<String> foreach_big() {

        List<String> result = new ArrayList<>(STRINGS_BIG.size());

        for (String name : STRINGS_BIG) {

            try(CloseableHttpClient httpClient = HttpClients.createDefault()) {
                final CloseableHttpResponse response = httpClient.execute(new HttpGet("https://httpbin.org/get?name=" + name));
                result.add(IOUtils.toString(response.getEntity().getContent()));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        return result;
    }

    @Benchmark
    public List<String> stream_small() {
        return STRINGS_SMALL.stream().unordered()
                .map(name -> {

                    try(CloseableHttpClient httpClient = HttpClients.createDefault()) {
                        final CloseableHttpResponse response = httpClient.execute(new HttpGet("https://httpbin.org/get?name=" + name));
                        return IOUtils.toString(response.getEntity().getContent());
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());
    }

    @Benchmark
    public List<String> stream_big() {
        return STRINGS_BIG.stream().unordered()
                .map(name -> {

                    try(CloseableHttpClient httpClient = HttpClients.createDefault()) {
                        final CloseableHttpResponse response = httpClient.execute(new HttpGet("https://httpbin.org/get?name=" + name));
                        return IOUtils.toString(response.getEntity().getContent());
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());
    }

    @Benchmark
    public List<String> stream_big_parallel() {
        return STRINGS_BIG.parallelStream().unordered()
                .map(name -> {

                    try(CloseableHttpClient httpClient = HttpClients.createDefault()) {
                        final CloseableHttpResponse response = httpClient.execute(new HttpGet("https://httpbin.org/get?name=" + name));
                        return IOUtils.toString(response.getEntity().getContent());
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());
    }
}
