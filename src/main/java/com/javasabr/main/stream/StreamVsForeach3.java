package com.javasabr.main.stream;

import static java.lang.Math.abs;
import com.ss.rlib.geom.Quaternion4f;
import com.ss.rlib.geom.Vector3f;
import org.openjdk.jmh.annotations.*;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10)
@Fork(value = 1)
@Measurement(iterations = 5)
public class StreamVsForeach3 {

    public static final List<GameObject> OBJECTS_SMALL = new ArrayList<>();
    public static final List<GameObject> OBJECTS_BIG = new ArrayList<>();

    static {

        final ThreadLocalRandom current = ThreadLocalRandom.current();

        for(int i = 0, length = 3; i < length; i++) {

            final Vector3f location = Vector3f.newInstance();
            location.set((float) current.nextDouble(), (float) current.nextDouble(), (float) current.nextDouble());

            final Quaternion4f quaternion4f = Quaternion4f.newInstance();
            quaternion4f.setXYZW((float) current.nextDouble(), (float) current.nextDouble(), (float) current.nextDouble(),  (float) current.nextDouble());

            OBJECTS_SMALL.add(new GameObject(location, quaternion4f, current.nextInt()));
        }

        for(int i = 0, length = 10000; i < length; i++) {

            final Vector3f location = Vector3f.newInstance();
            location.set((float) current.nextDouble(), (float) current.nextDouble(), (float) current.nextDouble());

            final Quaternion4f quaternion4f = Quaternion4f.newInstance();
            quaternion4f.setXYZW((float) current.nextDouble(), (float) current.nextDouble(), (float) current.nextDouble(),  (float) current.nextDouble());

            OBJECTS_BIG.add(new GameObject(location, quaternion4f, current.nextInt()));
        }
    }

    @Benchmark
    public List<Integer> classic_small() {

        final Set<GameObject> distinct = new HashSet<>();

        final Vector3f toCheckVector3f = Vector3f.UNIT_XYZ;
        final Quaternion4f toCheckQuaternion4f = Quaternion4f.newInstance().fromAngles(1, 0, 1);

        for (GameObject object : OBJECTS_SMALL) {
            distinct.add(object);
        }

        final List<GameObject> toSort = new ArrayList<>();

        for (GameObject gameObject : distinct) {

            final Vector3f location = gameObject.getLocation();

            if(location.distance(toCheckVector3f) < 0.1F) {
                continue;
            }

            location.addLocal(toCheckVector3f);

            final Quaternion4f quaternion4f = gameObject.getRotation();

            if(abs(quaternion4f.dot(toCheckQuaternion4f)) < 0.1F) {
                continue;
            }

            toSort.add(gameObject);
        }

        Collections.sort(toSort, (first, second) -> compare(first.getLocation(), second.getLocation(), toCheckVector3f));

        final List<Integer> objectIds = new ArrayList<>(toSort.size());

        for (GameObject gameObject : toSort) {
            objectIds.add(gameObject.getObjectId());
        }

        return objectIds;
    }

    @Benchmark
    public List<Integer> classic_big() {

        final Set<GameObject> distinct = new HashSet<>();

        final Vector3f toCheckVector3f = Vector3f.UNIT_XYZ;
        final Quaternion4f toCheckQuaternion4f = Quaternion4f.newInstance().fromAngles(1, 0, 1);

        for (GameObject object : OBJECTS_BIG) {
            distinct.add(object);
        }

        final List<GameObject> toSort = new ArrayList<>();

        for (GameObject gameObject : distinct) {

            final Vector3f location = gameObject.getLocation();

            if(location.distance(toCheckVector3f) < 0.1F) {
                continue;
            }

            location.addLocal(toCheckVector3f);

            final Quaternion4f quaternion4f = gameObject.getRotation();

            if(abs(quaternion4f.dot(toCheckQuaternion4f)) < 0.1F) {
                continue;
            }

            toSort.add(gameObject);
        }

        Collections.sort(toSort, (first, second) -> compare(first.getLocation(), second.getLocation(), toCheckVector3f));

        final List<Integer> objectIds = new ArrayList<>(toSort.size());

        for (GameObject gameObject : toSort) {
            objectIds.add(gameObject.getObjectId());
        }

        return objectIds;
    }

    @Benchmark
    public List<Integer> stream_small() {

        final Vector3f toCheckVector3f = Vector3f.UNIT_XYZ;
        final Quaternion4f toCheckQuaternion4f = Quaternion4f.newInstance().fromAngles(1, 0, 1);

        return OBJECTS_SMALL.stream().unordered()
                .distinct()
                .filter(object -> object.getLocation().distance(toCheckVector3f) < 0.1F)
                .filter(object -> abs(object.getRotation().dot(toCheckQuaternion4f)) < 0.1F)
                .sorted((first, second) -> compare(first.getLocation(), second.getLocation(), toCheckVector3f))
                .map(GameObject::getObjectId)
                .collect(Collectors.toList());
    }

    @Benchmark
    public List<Integer> stream_big() {

        final Vector3f toCheckVector3f = Vector3f.UNIT_XYZ;
        final Quaternion4f toCheckQuaternion4f = Quaternion4f.newInstance().fromAngles(1, 0, 1);

        return OBJECTS_BIG.stream().unordered()
                .distinct()
                .filter(object -> object.getLocation().distance(toCheckVector3f) < 0.1F)
                .filter(object -> abs(object.getRotation().dot(toCheckQuaternion4f)) < 0.1F)
                .sorted((first, second) -> compare(first.getLocation(), second.getLocation(), toCheckVector3f))
                .map(GameObject::getObjectId)
                .collect(Collectors.toList());
    }

    @Benchmark
    public List<Integer> stream_big_parallel() {

        final Vector3f toCheckVector3f = Vector3f.UNIT_XYZ;
        final Quaternion4f toCheckQuaternion4f = Quaternion4f.newInstance().fromAngles(1, 0, 1);

        return OBJECTS_BIG.parallelStream().unordered()
                .distinct()
                .filter(object -> object.getLocation().distance(toCheckVector3f) < 0.1F)
                .filter(object -> abs(object.getRotation().dot(toCheckQuaternion4f)) < 0.1F)
                .sorted((first, second) -> compare(first.getLocation(), second.getLocation(), toCheckVector3f))
                .map(GameObject::getObjectId)
                .collect(Collectors.toList());
    }

    private static int compare(Vector3f first, Vector3f second, Vector3f check) {

        final float firstDist = first.distance(check);
        final float secondDist = second.distance(check);

        if(firstDist > secondDist) {
            return 1;
        } else if(secondDist > firstDist) {
            return -1;
        }

        return 0;
    }
}
