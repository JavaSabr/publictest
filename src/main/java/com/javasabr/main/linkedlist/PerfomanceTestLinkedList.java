package com.javasabr.main.linkedlist;

import com.ss.rlib.util.linkedlist.LinkedListFactory;
import org.openjdk.jmh.annotations.Benchmark;

import java.util.Deque;

/**
 * Реализация теста на сравнение производительности LinkedList с FastLinkedList.
 * 
 * @author Ronn
 */
public class PerfomanceTestLinkedList {

	public static final int COLLECTION_SIZE = 10000000;

	@Benchmark
	public Object jdkLinkedListAdd() {

		Deque<Integer> collection = new java.util.LinkedList<>();

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
			collection.addFirst(i);
		}

		return collection;
	}

	@Benchmark
	public Object jdkLinkedListRemove() {

		Deque<Integer> collection = new java.util.LinkedList<>();

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
			collection.add(i);
		}

		while(!collection.isEmpty()) {
			collection.pollFirst();
		}

		return collection;
	}

	@Benchmark
	public Object jdkLinkedListAddRemoveAdd() {

		Deque<Integer> collection = new java.util.LinkedList<>();

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
			collection.add(i);
		}

		while(!collection.isEmpty()) {
			collection.pollFirst();
		}

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
			collection.add(i);
		}

		return collection;
	}

	@Benchmark
	public Object myLinkedListAdd() {

		Deque<Integer> collection = LinkedListFactory.newLinkedList(Integer.class);

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
			collection.addFirst(i);
		}

		return collection;
	}

	@Benchmark
	public Object myLinkedListRemove() {

		Deque<Integer> collection = LinkedListFactory.newLinkedList(Integer.class);

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
			collection.add(i);
		}

		while(!collection.isEmpty()) {
			collection.pollFirst();
		}

		return collection;
	}

	@Benchmark
	public Object myLinkedListAddRemoveAdd() {

		Deque<Integer> collection = LinkedListFactory.newLinkedList(Integer.class);

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
			collection.add(i);
		}

		while(!collection.isEmpty()) {
			collection.pollFirst();
		}

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
			collection.add(i);
		}

		return collection;
	}
}
