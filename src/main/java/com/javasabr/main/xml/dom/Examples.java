package com.javasabr.main.xml.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;

/**
 * Created by ronn on 13.09.16.
 */
public class Examples {

    private static final DocumentBuilderFactory BUILDER_FACTORY = DocumentBuilderFactory.newInstance();
    private static final TransformerFactory TRANSFORMER_FACTORY = TransformerFactory.newInstance();
    private static final DocumentBuilder DOCUMENT_BUILDER;

    static {
        try {
            DOCUMENT_BUILDER = BUILDER_FACTORY.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException();
        }
    }

    public static void main(final String[] args) throws SQLException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        foo3();
    }

    public static void foo1(Path file1, Path file2) throws IOException, SAXException {

        final Document first = DOCUMENT_BUILDER.parse(Files.newInputStream(file1));
        final Document second = DOCUMENT_BUILDER.parse(Files.newInputStream(file2));

        final Element firstRoot = first.getDocumentElement();
        firstRoot.appendChild(second.getDocumentElement());
    }

    public static void foo2(Path file1, Path file2) throws IOException, SAXException, TransformerException {

        final Document first = DOCUMENT_BUILDER.parse(Files.newInputStream(file1));

        final Transformer transformer = TRANSFORMER_FACTORY.newTransformer();
        //transformer.transform(first, Files.newOutputStream(file2));
    }

    public static void foo3() {

        final LocalDate now = LocalDate.now();
        final LocalDate before = LocalDate.now().minusDays(1);

        final Period period = Period.between(before, now);
        final Duration duration = Duration.between(before, now);

        System.out.println("period " + period + ", " + duration);
    }
}
