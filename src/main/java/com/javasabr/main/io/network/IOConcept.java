package com.javasabr.main.io.network;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class IOConcept {

   public static void foo() throws IOException {

       Socket socket = getSocket();

       // reading socket
       Scanner sc = new Scanner(socket.getInputStream());
       String string = sc.nextLine();
       System.out.println("Received " + string);


       // writing socket
       PrintWriter pw = new PrintWriter(socket.getOutputStream());
       pw.println("Hello");
   }

    private static Socket getSocket() {
        return null;
    }
}
