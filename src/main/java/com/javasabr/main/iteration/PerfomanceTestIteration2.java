package com.javasabr.main.iteration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openjdk.jmh.annotations.Benchmark;

/**
 * Реализация теста для демонстрации разных подходов к итерации коллекций.
 * 
 * @author Ronn
 */
public class PerfomanceTestIteration2 {

	public static final int COLLECTION_SIZE = 10000;
	public static final int ITERATION_COUNT = 100000;

	private static final List<Integer> jdkArrayList = new ArrayList<>();

	private static final Set<Integer> jdkHashSet = new HashSet<Integer>();

	private static final Object[] array = new Object[COLLECTION_SIZE];

	static {

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
			jdkArrayList.add(i);
		}

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
			jdkHashSet.add(i);
		}
	}

	@Benchmark
	public int iterationJDKArrayList() {

		final List<Integer> collection = jdkArrayList;

		int value = 0;

		for(int i = 0, length = ITERATION_COUNT; i < length; i++) {
			for(final Integer val : collection) {
				value += val.intValue();
			}
		}

		return value;
	}

	@Benchmark
	public int iterationJDKArrayNotWork() {

		final Object[] collection = array;

		for(int i = 0, length = ITERATION_COUNT; i < length; i++) {
			for(final Object val : collection) {
			}
		}

		return 0;
	}

	@Benchmark
	public int iterationJDKHashSet() {

		final Set<Integer> collection = jdkHashSet;

		int value = 0;

		for(int i = 0, length = ITERATION_COUNT; i < length; i++) {
			for(final Integer val : collection) {
				value += val.intValue();
			}
		}

		return value;
	}

}
