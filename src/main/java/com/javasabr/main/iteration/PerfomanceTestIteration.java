package com.javasabr.main.iteration;

import com.ss.rlib.util.array.Array;
import com.ss.rlib.util.array.ArrayFactory;
import com.ss.rlib.util.linkedlist.LinkedList;
import com.ss.rlib.util.linkedlist.LinkedListFactory;
import com.ss.rlib.util.linkedlist.impl.Node;
import org.openjdk.jmh.annotations.Benchmark;

import java.util.ArrayList;
import java.util.List;

/**
 * Реализация теста для демонстрации разных подходов к итерации коллекций.
 * 
 * @author Ronn
 */
public class PerfomanceTestIteration {

	public static final int COLLECTION_SIZE = 10000;
	public static final int ITERATION_COUNT = 100000;

	private static final List<Integer> jdkArrayList = new ArrayList<>();

	private static final List<Integer> jdkLinkedList = new java.util.LinkedList<>();

	private static final Array<Integer> myArray = ArrayFactory.newArray(Integer.class);

	private static final LinkedList<Integer> myLinkedList = LinkedListFactory.newLinkedList(Integer.class);

	static {

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
			jdkArrayList.add(i);
		}

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
			jdkLinkedList.add(i);
		}

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
			myArray.add(i);
		}

		for(int i = 0, length = COLLECTION_SIZE; i < length; i++) {
			myLinkedList.add(i);
		}
	}

	@Benchmark
	public int iterationJDKArrayList() {

		final List<Integer> collection = jdkArrayList;

		int value = 0;

		for(int i = 0, length = ITERATION_COUNT; i < length; i++) {
			for(final Integer val : collection) {
				value += val.intValue();
			}
		}

		return value;
	}

	@Benchmark
	public int iterationJDKLinkedList() {

		final List<Integer> collection = jdkLinkedList;

		int value = 0;

		for(int i = 0, length = ITERATION_COUNT; i < length; i++) {
			for(final Integer val : collection) {
				value += val.intValue();
			}
		}

		return value;
	}

	@Benchmark
	public int iterationMyArray() {

		final Array<Integer> collection = myArray;

		int value = 0;

		for(int i = 0, length = ITERATION_COUNT; i < length; i++) {
			for(final Integer val : collection.array()) {

				if(val == null) {
					break;
				}

				value += val.intValue();
			}
		}

		return value;
	}

	@Benchmark
	public int iterationMyLinkedList() {

		final LinkedList<Integer> collection = myLinkedList;

		int value = 0;

		for(int i = 0, length = ITERATION_COUNT; i < length; i++) {
			for(Node<Integer> node = collection.getFirstNode(); node != null; node = node.getNext()) {
				value += node.getItem();
			}
		}

		return value;
	}

}
