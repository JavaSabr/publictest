package com.javasabr.main.reuse;

import com.ss.rlib.concurrent.atomic.AtomicInteger;
import com.ss.rlib.geom.Vector3f;
import org.openjdk.jmh.annotations.Benchmark;

import java.util.ArrayList;
import java.util.List;

/**
 * Реализация теста с использование {@link ThreadLocal}.
 * 
 * @author Ronn
 */
public class ThreadLocalPerfomanceTest {

	public static final int PROP_INVOCATION_COUNT = 3000000;
	public static final int PROP_INVOCATION_COUNT_2 = 50000000;
	public static final int PROP_ADD_COUNT_ELEMENTS = 100;

	private static final ThreadLocal<List<Integer>> THREAD_LOCAL_COLLECTION = new ThreadLocal<List<Integer>>() {

		@Override
		protected List<Integer> initialValue() {
			return new ArrayList<>();
		}
	};

	private static final ThreadLocal<List<Vector3f>> THREAD_LOCAL_VECTORS = new ThreadLocal<List<Vector3f>>() {

		@Override
		protected List<Vector3f> initialValue() {

			final List<Vector3f> list = new ArrayList<Vector3f>();

			for(int i = 0, length = 20; i < length; i++) {
				list.add(Vector3f.newInstance());
			}

			return list;
		}
	};

	@Benchmark
	public Object testCollectionWithoutThreadLocal() {

		final Thread thread = new Thread(() -> {

			for(int i = 0, length = PROP_INVOCATION_COUNT; i < length; i++) {
				testCollectionWithoutThreadLocalImpl();
			}
		});

		thread.start();

		try {
			thread.join();
		} catch(final InterruptedException e) {
			e.printStackTrace();
		}

		return null;
	}

	protected Object testCollectionWithoutThreadLocalImpl() {

		final List<Integer> container = new ArrayList<>();

		for(int i = 0, length = PROP_ADD_COUNT_ELEMENTS; i < length; i++) {
			container.add(i);
		}

		return container;
	}

	@Benchmark
	public Object testCollectionWithThreadLocal() {

		final Thread thread = new Thread(() -> {

			for(int i = 0, length = PROP_INVOCATION_COUNT; i < length; i++) {
				testCollectionWithThreadLocalImpl();
			}
		});

		thread.start();

		try {
			thread.join();
		} catch(final InterruptedException e) {
			e.printStackTrace();
		}

		return null;
	}

	protected Object testCollectionWithThreadLocalImpl() {

		final List<Integer> container = THREAD_LOCAL_COLLECTION.get();
		container.clear();

		for(int i = 0, length = PROP_ADD_COUNT_ELEMENTS; i < length; i++) {
			container.add(i);
		}

		return container;
	}

	@Benchmark
	public float testVectorsWithoutThreadLocal() {

		final AtomicInteger integer = new AtomicInteger();

		final Thread thread = new Thread(() -> {

			float value = 0;

			for(int i = 0, length = PROP_INVOCATION_COUNT_2; i < length; i++) {
				value += testVectorsWithoutThreadLocalImpl(i);
			}

			integer.set((int) value);
		});

		thread.start();

		try {
			thread.join();
		} catch(final InterruptedException e) {
			e.printStackTrace();
		}

		return integer.get();
	}

	protected float testVectorsWithoutThreadLocalImpl(final int i) {

		final Vector3f vec1 = Vector3f.newInstance(i, i, i);
		final Vector3f vec2 = Vector3f.newInstance(i, i, i);
		final Vector3f vec3 = Vector3f.newInstance(i, i, i);
		final Vector3f vec4 = Vector3f.newInstance(i, i, i);
		final Vector3f vec5 = Vector3f.newInstance(i, i, i);
		final Vector3f vec6 = Vector3f.newInstance(i, i, i);

		vec1.addLocal(vec2).multLocal(vec3).crossLocal(vec4).multLocal(vec5).subtractLocal(vec6);

		return vec1.distance(vec6);
	}

	@Benchmark
	public float testVectorsWithThreadLocal() {

		final AtomicInteger integer = new AtomicInteger();

		final Thread thread = new Thread(() -> {

			float value = 0;

			for(int i = 0, length = PROP_INVOCATION_COUNT_2; i < length; i++) {
				value += testVectorsWithThreadLocalImpl(i);
			}

			integer.set((int) value);
		});

		thread.start();

		try {
			thread.join();
		} catch(final InterruptedException e) {
			e.printStackTrace();
		}

		return integer.get();
	}

	protected float testVectorsWithThreadLocalImpl(final int i) {

		final List<Vector3f> list = THREAD_LOCAL_VECTORS.get();

		final Vector3f vec1 = list.get(0);
		vec1.set(i, i, i);

		final Vector3f vec2 = list.get(1);
		vec2.set(i, i, i);

		final Vector3f vec3 = list.get(2);
		vec3.set(i, i, i);

		final Vector3f vec4 = list.get(3);
		vec4.set(i, i, i);

		final Vector3f vec5 = list.get(4);
		vec5.set(i, i, i);

		final Vector3f vec6 = list.get(5);
		vec6.set(i, i, i);

		vec1.addLocal(vec2).multLocal(vec3).crossLocal(vec4).multLocal(vec5).subtractLocal(vec6);
		return vec1.distance(vec6);
	}
}
