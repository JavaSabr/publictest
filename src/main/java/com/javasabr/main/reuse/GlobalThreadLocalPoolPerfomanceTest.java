package com.javasabr.main.reuse;

import com.ss.rlib.util.pools.PoolFactory;
import com.ss.rlib.util.pools.Reusable;
import com.ss.rlib.util.pools.ReusablePool;
import org.openjdk.jmh.annotations.*;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.TimeUnit;

/**
 * Реализация теста для демонстрации использования набора потоко-локальных
 * потоко-безопастных пулов для глобального переиспользования объектов.
 * 
 * @author Ronn
 */
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@State(Scope.Group)
@SuppressWarnings("unused")
public class GlobalThreadLocalPoolPerfomanceTest {

	public static final int THREAD_COUNT = 8;
	public static final int OBJECTS_SIZE = 10000;
	public static final int OBJECTS_SIZE_THREAD = OBJECTS_SIZE / (THREAD_COUNT + 1);

	public static class BigObject implements Reusable {

		private final Map<String, Object> map1 = new HashMap<>(100);
		private final Map<String, Object> map2 = new HashMap<>(100);
		private final Map<String, Object> map3 = new HashMap<>(100);
		private final Map<String, Object> map4 = new HashMap<>(100);
		private final Map<String, Object> map5 = new HashMap<>(100);
		private final Map<String, Object> map6 = new HashMap<>(100);
		private final Map<String, Object> map7 = new HashMap<>(100);

		private final Set<String> set1 = new HashSet<>(100);
		private final Set<String> set2 = new HashSet<>(100);
		private final Set<String> set3 = new HashSet<>(100);
		private final Set<String> set4 = new HashSet<>(100);
		private final Set<String> set5 = new HashSet<>(100);
		private final Set<String> set6 = new HashSet<>(100);
		private final Set<String> set7 = new HashSet<>(100);

		private final List<Integer> list1 = new ArrayList<>(200);
		private final List<Integer> list2 = new ArrayList<>(200);
		private final List<Integer> list3 = new ArrayList<>(200);
		private final List<Integer> list4 = new ArrayList<>(200);

		private volatile ReusablePool<BigObject> pool;

		private Object ref1;
		private Object ref2;
		private Object ref3;
		private Object ref4;
		private Object ref5;
		private Object ref6;
		private Object ref7;
		private Object ref8;
		private Object ref9;
		private Object ref10;
		private Object ref11;
		private Object ref12;
		private Object ref13;

		private double db1;
		private double db2;
		private double db3;
		private double db4;
		private double db5;
		private double db6;
		private double db7;
		private double db8;
		private double db9;
		private double db10;
		private double db11;
		private double db12;

		public void setPool(final ReusablePool<BigObject> pool) {
			this.pool = pool;
		}

		public ReusablePool<BigObject> getPool() {
			return pool;
		}

		@Override
		public void release() {
			getPool().put(this);
		}
	}

	private static final ThreadLocal<ReusablePool<BigObject>> LOCAL_POOL = new ThreadLocal<ReusablePool<BigObject>>() {

		@Override
		protected ReusablePool<BigObject> initialValue() {
			return PoolFactory.newReusablePool(BigObject.class);
		}
	};

	private static final Deque<BigObject> FIRST_LIST = new ConcurrentLinkedDeque<BigObject>();
	private static final Deque<BigObject> SECOND_LIST = new ConcurrentLinkedDeque<BigObject>();

	@Benchmark
	@Group("testCreateObjectWithoutPoolMaxConcurency")
	@GroupThreads(THREAD_COUNT)
	public Object test1CreateObjectWithoutPoolMaxConcurency() {

		final List<BigObject> objects = new ArrayList<>(OBJECTS_SIZE_THREAD);

		for(int i = 0, length = OBJECTS_SIZE_THREAD; i < length; i++) {
			objects.add(new BigObject());
		}

		FIRST_LIST.addAll(objects);

		return objects;
	}

	@Benchmark
	@Group("testCreateObjectWithPoolMaxConcurency")
	@GroupThreads(THREAD_COUNT)
	public Object test1CreateObjectWithPoolMaxConcurency() {

		final ReusablePool<BigObject> pool = LOCAL_POOL.get();
		final BigObject check = pool.take();

		if(check == null) {
			for(int i = 0, length = OBJECTS_SIZE; i < length; i++) {
				pool.put(new BigObject());
			}

			System.out.println("Create from " + Thread.currentThread());
		}

		final List<BigObject> objects = new ArrayList<>(OBJECTS_SIZE_THREAD);

		for(int i = 0, length = OBJECTS_SIZE_THREAD; i < length; i++) {

			BigObject object = pool.take();

			if(object == null) {
				object = new BigObject();
			}

			object.setPool(pool);

			objects.add(object);
		}

		SECOND_LIST.addAll(objects);

		return objects;
	}

	@Benchmark
	@Group("testCreateObjectWithoutPoolMaxConcurency")
	@GroupThreads(THREAD_COUNT)
	public Object testClear2ListWithoutPoolMaxConcurency() {

		for(BigObject object = FIRST_LIST.pollLast(); object != null; object = FIRST_LIST.pollLast());

		return null;
	}

	@Benchmark
	@Group("testCreateObjectWithPoolMaxConcurency")
	@GroupThreads(THREAD_COUNT)
	public Object test2ClearQueueWithPoolMaxConcurency() {

		for(BigObject object = SECOND_LIST.pollLast(); object != null; object = SECOND_LIST.pollLast()) {
			object.release();
		}

		return null;
	}
}
