package com.javasabr.main.reuse;

import com.ss.rlib.util.pools.PoolFactory;
import com.ss.rlib.util.pools.Reusable;
import com.ss.rlib.util.pools.ReusablePool;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

import java.util.*;

/**
 * Реализация теста для демонстрации использования глобального не
 * потоко-безопастного пула в однопоточном процессе.
 * 
 * @author Ronn
 */
@State(Scope.Thread)
@SuppressWarnings("unused")
public class GlobalThreadUnsafePoolPerfomanceTest {

	public static final int OBJECTS_SIZE = 130000;

	public static class BigObject implements Reusable {

		private final Map<String, Object> map1 = new HashMap<>(100);
		private final Map<String, Object> map2 = new HashMap<>(100);
		private final Map<String, Object> map3 = new HashMap<>(100);
		private final Map<String, Object> map4 = new HashMap<>(100);
		private final Map<String, Object> map5 = new HashMap<>(100);
		private final Map<String, Object> map6 = new HashMap<>(100);
		private final Map<String, Object> map7 = new HashMap<>(100);

		private final Set<String> set1 = new HashSet<>(100);
		private final Set<String> set2 = new HashSet<>(100);
		private final Set<String> set3 = new HashSet<>(100);
		private final Set<String> set4 = new HashSet<>(100);
		private final Set<String> set5 = new HashSet<>(100);
		private final Set<String> set6 = new HashSet<>(100);
		private final Set<String> set7 = new HashSet<>(100);

		private final List<Integer> list1 = new ArrayList<>(200);
		private final List<Integer> list2 = new ArrayList<>(200);
		private final List<Integer> list3 = new ArrayList<>(200);
		private final List<Integer> list4 = new ArrayList<>(200);

		private Object ref1;
		private Object ref2;
		private Object ref3;
		private Object ref4;
		private Object ref5;
		private Object ref6;
		private Object ref7;
		private Object ref8;
		private Object ref9;
		private Object ref10;
		private Object ref11;
		private Object ref12;
		private Object ref13;

		private double db1;
		private double db2;
		private double db3;
		private double db4;
		private double db5;
		private double db6;
		private double db7;
		private double db8;
		private double db9;
		private double db10;
		private double db11;
		private double db12;
	}

	private static final ReusablePool<BigObject> THREAD_UNSAFE_POOL = PoolFactory.newReusablePool(BigObject.class);

	static {
		for(int i = 0, length = OBJECTS_SIZE; i < length; i++) {
			THREAD_UNSAFE_POOL.put(new BigObject());
		}
	}

	@Benchmark
	public Object testWithoutPool() {

		final List<BigObject> objects = new ArrayList<>(OBJECTS_SIZE);

		for(int i = 0, length = OBJECTS_SIZE; i < length; i++) {
			objects.add(new BigObject());
		}

		objects.clear();

		for(int i = 0, length = OBJECTS_SIZE; i < length; i++) {
			objects.add(new BigObject());
		}

		objects.clear();

		return objects;
	}

	@Benchmark
	public Object testWithPool() {

		final List<BigObject> objects = new ArrayList<>(OBJECTS_SIZE);

		for(int i = 0, length = OBJECTS_SIZE; i < length; i++) {

			BigObject object = THREAD_UNSAFE_POOL.take();

			if(object == null) {
				object = new BigObject();
			}

			objects.add(object);
		}

		for(final BigObject bigObject : objects) {
			THREAD_UNSAFE_POOL.put(bigObject);
		}

		objects.clear();

		for(int i = 0, length = OBJECTS_SIZE; i < length; i++) {

			BigObject object = THREAD_UNSAFE_POOL.take();

			if(object == null) {
				object = new BigObject();
			}

			objects.add(object);
		}

		for(final BigObject bigObject : objects) {
			THREAD_UNSAFE_POOL.put(bigObject);
		}

		objects.clear();

		return objects;
	}
}
