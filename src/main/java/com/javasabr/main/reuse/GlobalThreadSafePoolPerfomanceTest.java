package com.javasabr.main.reuse;

import com.ss.rlib.util.pools.PoolFactory;
import com.ss.rlib.util.pools.Reusable;
import com.ss.rlib.util.pools.ReusablePool;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Реализация теста для демонстрации использования глобального
 * потоко-безопастного пула в многопоточной среде.
 * 
 * @author Ronn
 */
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@State(Scope.Group)
@SuppressWarnings("unused")
public class GlobalThreadSafePoolPerfomanceTest {

	public static final int THREAD_COUNT = 16;
	public static final int OBJECTS_SIZE = 100000;
	public static final int OBJECTS_SIZE_THREAD = OBJECTS_SIZE / (THREAD_COUNT + 1);

	public static class BigObject implements Reusable {

		private final Map<String, Object> ap1 = new HashMap<>(100);
		private final Map<String, Object> map2 = new HashMap<>(100);
		private final Map<String, Object> map3 = new HashMap<>(100);
		private final Map<String, Object> map4 = new HashMap<>(100);
		private final Map<String, Object> map5 = new HashMap<>(100);
		private final Map<String, Object> map6 = new HashMap<>(100);
		private final Map<String, Object> map7 = new HashMap<>(100);

		private final Set<String> set1 = new HashSet<>(100);
		private final Set<String> set2 = new HashSet<>(100);
		private final Set<String> set3 = new HashSet<>(100);
		private final Set<String> set4 = new HashSet<>(100);
		private final Set<String> set5 = new HashSet<>(100);
		private final Set<String> set6 = new HashSet<>(100);
		private final Set<String> set7 = new HashSet<>(100);

		private final List<Integer> list1 = new ArrayList<>(200);
		private final List<Integer> list2 = new ArrayList<>(200);
		private final List<Integer> list3 = new ArrayList<>(200);
		private final List<Integer> list4 = new ArrayList<>(200);

		private Object ref1;
		private Object ref2;
		private Object ref3;
		private Object ref4;
		private Object ref5;
		private Object ref6;
		private Object ref7;
		private Object ref8;
		private Object ref9;
		private Object ref10;
		private Object ref11;
		private Object ref12;
		private Object ref13;

		private double db1;
		private double db2;
		private double db3;
		private double db4;
		private double db5;
		private double db6;
		private double db7;
		private double db8;
		private double db9;
		private double db10;
		private double db11;
		private double db12;
	}

	private static final ReusablePool<BigObject> THREAD_SAFE_POOL = PoolFactory.newConcurrentAtomicARSWLockReusablePool(BigObject.class);

	static {
		for(int i = 0, length = OBJECTS_SIZE; i < length; i++) {
			THREAD_SAFE_POOL.put(new BigObject());
		}
	}

	@Benchmark
	@Group("testWithoutPoolMaxConcurency")
	@GroupThreads(THREAD_COUNT)
	public Object testWithoutPoolMaxConcurency() {

		final List<BigObject> objects = new ArrayList<>(OBJECTS_SIZE_THREAD);

		for(int i = 0, length = OBJECTS_SIZE_THREAD; i < length; i++) {
			objects.add(new BigObject());
		}

		objects.clear();

		return objects;
	}

	@Benchmark
	@Group("testWithPoolMaxConcurency")
	@GroupThreads(THREAD_COUNT)
	public Object testWithPoolMaxConcurency() {

		final List<BigObject> objects = new ArrayList<>(OBJECTS_SIZE_THREAD);

		for(int i = 0, length = OBJECTS_SIZE_THREAD; i < length; i++) {

			BigObject object = THREAD_SAFE_POOL.take();

			if(object == null) {
				object = new BigObject();
			}

			objects.add(object);
		}

		for(final BigObject bigObject : objects) {
			THREAD_SAFE_POOL.put(bigObject);
		}

		objects.clear();

		return objects;
	}

	@Benchmark
	@Group("testWithoutPoolMinConcurency")
	@GroupThreads(THREAD_COUNT)
	public Object testWithoutPoolMinConcurency() {

		final List<BigObject> objects = new ArrayList<>(OBJECTS_SIZE_THREAD / 10);

		for(int i = 0, length = OBJECTS_SIZE_THREAD / 10; i < length; i++) {
			objects.add(new BigObject());
			Blackhole.consumeCPU((long) Math.log(Math.PI));
		}

		int count = 0;

		for(final BigObject bigObject : objects) {
			count++;
			Blackhole.consumeCPU((long) Math.log(Math.PI));
		}

		objects.clear();

		return count > 0 ? objects : null;
	}

	protected void sleep() {
		try {
			Thread.sleep(0, 500);
		} catch(final InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Benchmark
	@Group("testWithPoolMinConcurency")
	@GroupThreads(THREAD_COUNT)
	public Object testWithPoolMinConcurency() {

		final List<BigObject> objects = new ArrayList<>(OBJECTS_SIZE_THREAD / 10);

		for(int i = 0, length = OBJECTS_SIZE_THREAD / 10; i < length; i++) {

			BigObject object = THREAD_SAFE_POOL.take();

			if(object == null) {
				object = new BigObject();
			}

			objects.add(object);

			Blackhole.consumeCPU((long) Math.log(Math.PI));
		}

		int count = 0;

		for(final BigObject bigObject : objects) {

			THREAD_SAFE_POOL.put(bigObject);

			count++;

			Blackhole.consumeCPU((long) Math.log(Math.PI));
		}

		objects.clear();

		return objects;
	}
}
