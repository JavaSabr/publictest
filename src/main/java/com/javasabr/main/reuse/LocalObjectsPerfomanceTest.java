package com.javasabr.main.reuse;

import com.ss.rlib.concurrent.atomic.AtomicInteger;
import com.ss.rlib.geom.Vector3f;
import org.openjdk.jmh.annotations.Benchmark;

import java.util.ArrayList;
import java.util.List;

/**
 * Реализация теста с использование подхода вынесения контейнера локальных
 * объектов в поле потока.
 * 
 * @author Ronn
 */
public class LocalObjectsPerfomanceTest {

	public static final int PROP_INVOCATION_COUNT = 3000000;
	public static final int PROP_INVOCATION_COUNT_2 = 50000000;
	public static final int PROP_ADD_COUNT_ELEMENTS = 100;

	/**
	 * Пример контейнера локальных объектов.
	 * 
	 * @author Ronn
	 */
	private static class LocalObjects {

		public static LocalObjects get() {
			return ((TestThread) Thread.currentThread()).getLocal();
		}

		private final List<Integer> collection;

		private final Vector3f[] vectors = new Vector3f[20];

		private int nextVectorIndex;

		public LocalObjects() {
			collection = new ArrayList<>();

			for(int i = 0, length = vectors.length; i < length; i++) {
				vectors[i] = Vector3f.newInstance();
			}
		}

		public List<Integer> getCollection() {
			return collection;
		}

		public Vector3f getNextVector() {

			nextVectorIndex++;

			if(vectors.length == nextVectorIndex) {
				nextVectorIndex = 0;
			}

			return vectors[nextVectorIndex];
		}
	}

	/**
	 * Пример переопределенного потока.
	 * 
	 * @author Ronn
	 */
	private static class TestThread extends Thread {

		private final LocalObjects local = new LocalObjects();

		public TestThread(final Runnable target) {
			super(target);
		}

		public LocalObjects getLocal() {
			return local;
		}
	}

	@Benchmark
	public Object testCollectionWithoutLocal() {

		final TestThread thread = new TestThread(() -> {

			for(int i = 0, length = PROP_INVOCATION_COUNT; i < length; i++) {
				testCollectionWithoutLocalImpl();
			}
		});

		thread.start();

		try {
			thread.join();
		} catch(final InterruptedException e) {
			e.printStackTrace();
		}

		return null;
	}

	protected Object testCollectionWithoutLocalImpl() {

		final List<Integer> container = new ArrayList<>();

		for(int i = 0, length = PROP_ADD_COUNT_ELEMENTS; i < length; i++) {
			container.add(i);
		}

		return container;
	}

	@Benchmark
	public Object testCollectionWithLocal() {

		final TestThread thread = new TestThread(() -> {

			for(int i = 0, length = PROP_INVOCATION_COUNT; i < length; i++) {
				testCollectionWithLocalImpl();
			}
		});

		thread.start();

		try {
			thread.join();
		} catch(final InterruptedException e) {
			e.printStackTrace();
		}

		return null;
	}

	protected Object testCollectionWithLocalImpl() {

		final LocalObjects localObjects = LocalObjects.get();
		final List<Integer> container = localObjects.getCollection();
		container.clear();

		for(int i = 0, length = PROP_ADD_COUNT_ELEMENTS; i < length; i++) {
			container.add(i);
		}

		return container;
	}

	@Benchmark
	public float testVectorsWithoutLocal() {

		final AtomicInteger integer = new AtomicInteger();

		final TestThread thread = new TestThread(() -> {

			float value = 0;

			for(int i = 0, length = PROP_INVOCATION_COUNT_2; i < length; i++) {
				value += testVectorsWithoutLocalImpl(i);
			}

			integer.set((int) value);
		});

		thread.start();

		try {
			thread.join();
		} catch(final InterruptedException e) {
			e.printStackTrace();
		}

		return integer.get();
	}

	protected float testVectorsWithoutLocalImpl(final int i) {

		final Vector3f vec1 = Vector3f.newInstance(i, i, i);
		final Vector3f vec2 = Vector3f.newInstance(i, i, i);
		final Vector3f vec3 = Vector3f.newInstance(i, i, i);
		final Vector3f vec4 = Vector3f.newInstance(i, i, i);
		final Vector3f vec5 = Vector3f.newInstance(i, i, i);
		final Vector3f vec6 = Vector3f.newInstance(i, i, i);

		vec1.addLocal(vec2).multLocal(vec3).crossLocal(vec4).multLocal(vec5).subtractLocal(vec6);

		return vec1.distance(vec6);
	}

	@Benchmark
	public float testVectorsWithLocal() {

		final AtomicInteger integer = new AtomicInteger();

		final TestThread thread = new TestThread(() -> {

			float value = 0;

			for(int i = 0, length = PROP_INVOCATION_COUNT_2; i < length; i++) {
				value += testVectorsWithLocalImpl(i);
			}

			integer.set((int) value);
		});

		thread.start();

		try {
			thread.join();
		} catch(final InterruptedException e) {
			e.printStackTrace();
		}

		return integer.get();
	}

	protected float testVectorsWithLocalImpl(final int i) {

		final LocalObjects localObjects = LocalObjects.get();

		final Vector3f vec1 = localObjects.getNextVector();
		vec1.set(i, i, i);

		final Vector3f vec2 = localObjects.getNextVector();
		vec2.set(i, i, i);

		final Vector3f vec3 = localObjects.getNextVector();
		vec3.set(i, i, i);

		final Vector3f vec4 = localObjects.getNextVector();
		vec4.set(i, i, i);

		final Vector3f vec5 = localObjects.getNextVector();
		vec5.set(i, i, i);

		final Vector3f vec6 = localObjects.getNextVector();
		vec6.set(i, i, i);

		vec1.addLocal(vec2).multLocal(vec3).crossLocal(vec4).multLocal(vec5).subtractLocal(vec6);
		return vec1.distance(vec6);
	}

	@Benchmark
	public float testVectorsWithLocal2() {

		final AtomicInteger integer = new AtomicInteger();

		final TestThread thread = new TestThread(() -> {

			float value = 0;

			final LocalObjects localObjects = LocalObjects.get();

			for(int i = 0, length = PROP_INVOCATION_COUNT_2; i < length; i++) {
				value += testVectorsWithLocalImpl2(localObjects, i);
			}

			integer.set((int) value);
		});

		thread.start();

		try {
			thread.join();
		} catch(final InterruptedException e) {
			e.printStackTrace();
		}

		return integer.get();
	}

	protected float testVectorsWithLocalImpl2(final LocalObjects localObjects, final int i) {

		final Vector3f vec1 = localObjects.getNextVector();
		vec1.set(i, i, i);

		final Vector3f vec2 = localObjects.getNextVector();
		vec2.set(i, i, i);

		final Vector3f vec3 = localObjects.getNextVector();
		vec3.set(i, i, i);

		final Vector3f vec4 = localObjects.getNextVector();
		vec4.set(i, i, i);

		final Vector3f vec5 = localObjects.getNextVector();
		vec5.set(i, i, i);

		final Vector3f vec6 = localObjects.getNextVector();
		vec6.set(i, i, i);

		vec1.addLocal(vec2).multLocal(vec3).crossLocal(vec4).multLocal(vec5).subtractLocal(vec6);
		return vec1.distance(vec6);
	}
}
