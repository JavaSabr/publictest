package com.javasabr.main.nio.network;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by ronn on 20.05.16.
 */
public class NIO1Concept {

    public static void foo() throws IOException {

        ByteBuffer readBuffer = ByteBuffer.allocate(1024);
        // prepare to read
        readBuffer.clear();

        SocketChannel channel = getChannel();
        channel.read(readBuffer);

        readBuffer.flip();
        // reading the buffer
        // ..................

        ByteBuffer writeBuffer = ByteBuffer.allocate(1024);
        // prepare to put data
        writeBuffer.clear();
        // putting the data
        // ...............
        // prepare to write
        writeBuffer.flip();

        channel.write(writeBuffer);
    }


    public static void foo2() throws IOException, InterruptedException {

        ServerSocketChannel channel = ServerSocketChannel.open();
        channel.bind(new InetSocketAddress("localhost", 2222));
        channel.configureBlocking(false);

        Selector selector = Selector.open();
        SelectionKey key = channel.register(selector, SelectionKey.OP_ACCEPT);

        while (true) {

            if (selector.select() == 0) {
                Thread.sleep(1);
                continue;
            }

            Set<SelectionKey> selectedKeys = selector.selectedKeys();
            Iterator<SelectionKey> keyIterator = selectedKeys.iterator();

            while (keyIterator.hasNext()) {

                SelectionKey selectionKey = keyIterator.next();

                if (selectionKey.isAcceptable()) {
                    // a connection was accepted by a ServerSocketChannel.
                } else if (selectionKey.isConnectable()) {
                    // a connection was established with a remote server.
                } else if (selectionKey.isReadable()) {
                    // a channel is ready for reading
                } else if (selectionKey.isWritable()) {
                    // a channel is ready for writing
                }

                keyIterator.remove();
            }
        }
    }

    private static SocketChannel getChannel() {
        return null;
    }
}
