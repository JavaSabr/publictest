package com.javasabr.main.nio.network;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.nio.channels.SocketChannel;
import java.util.concurrent.Executors;

import static com.javasabr.main.nio.network.NUtils.toHandler;

/**
 * Created by ronn on 20.05.16.
 */
public class NIO2Concept {

    public static void foo() throws IOException {

        ByteBuffer readBuffer = ByteBuffer.allocate(1024);
        // prepare to read
        readBuffer.clear();

        AsynchronousSocketChannel channel = getChannel2();
        channel.read(readBuffer, null, new CompletionHandler<Integer, Object>() {

            @Override
            public void completed(Integer result, Object attachment) {
                // buffer is ready for read
            }

            @Override
            public void failed(Throwable exc, Object attachment) {
                exc.printStackTrace();
            }
        });

        ByteBuffer writeBuffer = ByteBuffer.allocate(1024);
        // prepare to put data
        writeBuffer.clear();
        // putting the data
        // ...............
        // prepare to write
        writeBuffer.flip();

        channel.write(writeBuffer, null, new CompletionHandler<Integer, Object>() {

            @Override
            public void completed(Integer result, Object attachment) {
                // writing has been completed
            }

            @Override
            public void failed(Throwable exc, Object attachment) {
                exc.printStackTrace();
            }
        });
    }


    public static void foo2() throws IOException, InterruptedException {

        AsynchronousChannelGroup group = AsynchronousChannelGroup.withThreadPool(Executors.newFixedThreadPool(10));
        AsynchronousServerSocketChannel channel = AsynchronousServerSocketChannel.open(group);
        channel.bind(new InetSocketAddress("localhost", 2222));
        channel.accept(null, toHandler((client, attach) -> {

            ByteBuffer readBuffer = ByteBuffer.allocate(1024);
            ByteBuffer writeBuffer = ByteBuffer.allocate(1024);

            client.read(readBuffer, null, toHandler((result, attachment) -> {
                // buffer is ready for read
            }));

            client.write(writeBuffer, null, toHandler((result, attachment) -> {
                // writing has been completed
            }));
        }));
    }

    private static SocketChannel getChannel() {
        return null;
    }

    private static AsynchronousSocketChannel getChannel2() {
        return null;
    }
}
