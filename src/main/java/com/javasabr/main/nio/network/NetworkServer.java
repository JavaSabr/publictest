package com.javasabr.main.nio.network;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

import static com.javasabr.main.nio.network.NUtils.toHandler;
import static java.lang.System.out;
import static java.nio.channels.AsynchronousChannelGroup.withFixedThreadPool;

/**
 * Created by ronn on 22.05.16.
 */
public class NetworkServer {

    AsynchronousServerSocketChannel channel;
    CompletionHandler<AsynchronousSocketChannel, Void> handler;

    public NetworkServer(SocketAddress address) throws IOException {

        AsynchronousChannelGroup group = withFixedThreadPool(10, Thread::new);

        handler = toHandler((channel, attach) -> processAccept(channel));
        channel = AsynchronousServerSocketChannel.open(group);
        channel.bind(address);
        channel.accept(null, toHandler((clientChannel, attach) -> processAccept(clientChannel)));
    }

    private void processAccept(AsynchronousSocketChannel clientChannel) {
        NetworkClient networkClient = new NetworkClient(clientChannel, message -> out.println("Server: received: " + message));
        networkClient.write("Hello! I'm NIO.2 server!\n");
        channel.accept(null, handler);
    }

    public void stop() throws IOException {
        channel.close();
    }
}
