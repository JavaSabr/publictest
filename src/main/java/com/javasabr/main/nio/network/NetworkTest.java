package com.javasabr.main.nio.network;

import static com.javasabr.main.nio.network.NUtils.*;
import static java.lang.Thread.currentThread;
import com.ss.rlib.concurrent.util.ConcurrentUtils;
import com.ss.rlib.concurrent.util.ThreadUtils;
import org.openjdk.jmh.annotations.*;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10)
@Fork(value = 1)
@Measurement(iterations = 5)
public class NetworkTest {

    public static final int CLIENT_COUNT = 10;

    public static void main(String[] args) {
        new NetworkTest().test();
    }

    @Benchmark
    public void test() {

        AtomicInteger counter = new AtomicInteger(CLIENT_COUNT);

        new Thread(run(() -> {
            final NetworkServer server = new NetworkServer(new InetSocketAddress(3333));
            ConcurrentUtils.wait(counter);
            server.stop();
        })).start();

        ThreadUtils.sleep(1000);

        for (int i = 0, length = CLIENT_COUNT; i < length; i++) {
            new Thread(run(() -> {

                Socket socket = new Socket();
                socket.connect(new InetSocketAddress(3333));

                writeLine(socket, "Hello! I'm client " + currentThread().getName());
                System.out.println("Client: received: " + readLine(socket));

                synchronized (counter) {
                    if (counter.decrementAndGet() == 0) {
                        ConcurrentUtils.notifyAll(counter);
                    } else {
                        ConcurrentUtils.wait(counter);
                    }
                }
                socket.close();
            })).start();
        }

        synchronized (counter) {
            if (counter.get() > 0) {
                ConcurrentUtils.waitInSynchronize(counter);
            }
        }
    }
}
