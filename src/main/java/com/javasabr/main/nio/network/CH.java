package com.javasabr.main.nio.network;

import java.nio.channels.CompletionHandler;

/**
 * Created by ronn on 22.05.16.
 */
public interface CH<V, A> extends CompletionHandler<V, A> {

    @Override
    default void failed(Throwable exc, Object attachment) {
    }
}
