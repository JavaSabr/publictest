package com.javasabr.main.concurrent;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by ronn on 26.06.16.
 */
public class Multithreading {

    public static final String[] URLS = {
            "http://localhost/video1.mp4", "http://localhost/video2.mp4",
            "http://localhost/video3.mp4", "http://localhost/video4.mp4"
    };

    public void singleThread() throws IOException {
        for (final String urlPath : URLS) {
            final URL url = new URL(urlPath);
            final Path file = Files.createTempFile("pr", "sf");
            try (final InputStream in = url.openStream()) {
                Files.copy(in, file);
            }
        }
    }

    public void multiThread() throws IOException {
        for (final String urlPath : URLS) {
            new Thread(() -> {
                try {
                    final URL url = new URL(urlPath);
                    final Path file = Files.createTempFile("pr", "sf");
                    try (final InputStream in = url.openStream()) {
                        Files.copy(in, file);
                    }
                } catch (final Exception e) {
                    throw new RuntimeException(e);
                }
            }).start();
        }
    }

    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(4);

    public void multiThread2() throws IOException {
        for (final String urlPath : URLS) {
            EXECUTOR.submit(() -> {
                final URL url = new URL(urlPath);
                final Path file = Files.createTempFile("pr", "sf");
                try (final InputStream in = url.openStream()) {
                    Files.copy(in, file);
                }
                return null;
            });
        }
    }

    private static final ScheduledExecutorService SCHEDULED_EXECUTOR = Executors.newScheduledThreadPool(4);

    public void multiThread3() throws IOException {
        for (final String urlPath : URLS) {
            SCHEDULED_EXECUTOR.schedule(() -> {
                final URL url = new URL(urlPath);
                final Path file = Files.createTempFile("pr", "sf");
                try (final InputStream in = url.openStream()) {
                    Files.copy(in, file);
                }
                return null;
            }, 10, TimeUnit.SECONDS);
        }
    }
}
