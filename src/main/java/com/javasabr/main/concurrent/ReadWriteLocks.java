package com.javasabr.main.concurrent;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.StampedLock;

/**
 * Created by ronn on 27.06.16.
 */
public class ReadWriteLocks {

    private static final List<MouseListener> MOUSE_LISTENERS = new ArrayList<>();
    private static final StampedLock LOCK = new StampedLock();

    public static void addListener(final MouseListener listener) {
        final long stamp = LOCK.writeLock();
        try {
            MOUSE_LISTENERS.add(listener);
        } finally {
            LOCK.unlockWrite(stamp);
        }
    }

    public static void removeListener(final MouseListener listener) {
        final long stamp = LOCK.writeLock();
        try {
            MOUSE_LISTENERS.remove(listener);
        } finally {
            LOCK.unlockWrite(stamp);
        }
    }

    public static void notify(final MouseEvent event) {
        final long stamp = LOCK.readLock();
        try {
            MOUSE_LISTENERS.forEach(listener -> listener.mouseClicked(event));
        } finally {
            LOCK.unlockRead(stamp);
        }
    }
}
