package com.javasabr.main.concurrent;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ronn on 26.06.16.
 */
public class ThreadLocalExamples {

    private static final ThreadLocal<DateFormat> LOCAL_DATE_FORMAT = ThreadLocal.withInitial(DateFormat::getInstance);
    private static final ThreadLocal<Random> LOCAL_RANDOM = ThreadLocal.withInitial(Random::new);

    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(10);

    public void multiThread() throws IOException {
        for (int i = 0, length = 100; i < length; i++) {
            EXECUTOR.execute(() -> {
                final DateFormat dateFormat = LOCAL_DATE_FORMAT.get();
                final Random random = LOCAL_RANDOM.get();
                System.out.println(dateFormat.format(new Date(random.nextInt())));
            });
        }
    }
}
