package com.javasabr.main.concurrent;

/**
 * Created by ronn on 26.06.16.
 */
public class VolatileExample {

    private static volatile VolatileExample example;

    public static VolatileExample getExample() {
        if(example != null) return example;
        synchronized (VolatileExample.class) {
            if(example != null) return example;
            example = new VolatileExample();
        }
        return example;
    }

    private static volatile boolean BOOL = false;

    public void startThreads() {
        new Thread(() -> {
            while(true) {
                if(!BOOL) {
                    System.out.println("switch flag to true");
                    BOOL = true;
                }
            }
        }).start();
        new Thread(() -> {
            while(true) {
                if(BOOL) {
                    System.out.println("switch flag to false");
                    BOOL = false;
                }
            }
        }).start();
    }
}
