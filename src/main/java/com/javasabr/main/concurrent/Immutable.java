package com.javasabr.main.concurrent;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.System.currentTimeMillis;

/**
 * Created by ronn on 26.06.16.
 */
public class Immutable {

    private static final String BASE_STRING = "BaseString";
    private static final Integer BASE_INT = 20;

    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(10);

    public void multiThread() throws IOException {
        for(int i = 0, length = 100; i < length; i++) {
            EXECUTOR.execute(() ->
                    System.out.println(BASE_STRING + "_" + BASE_INT + "_" + currentTimeMillis()));
        }
    }
}
