package com.javasabr.main.concurrent;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by ronn on 27.06.16.
 */
public class Atomics {

    private static final AtomicInteger INTEGER = new AtomicInteger();

    public void startThreads() {
        new Thread(() -> {
            while(true) {
                if(INTEGER.compareAndSet(0, 1)) {
                    System.out.println("switch value to 1");
                }
            }
        }).start();
        new Thread(() -> {
            while(true) {
                if(INTEGER.compareAndSet(0, 1)) {
                    System.out.println("switch value to 1");
                }
            }
        }).start();
        new Thread(() -> {
            while(true) {
                if(INTEGER.compareAndSet(1, 0)) {
                    System.out.println("switch flag to 0");
                }
            }
        }).start();
        new Thread(() -> {
            while(true) {
                if(INTEGER.compareAndSet(1, 0)) {
                    System.out.println("switch flag to 0");
                }
            }
        }).start();
    }
}
