package com.javasabr.main.concurrent;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by ronn on 26.06.16.
 */
public class MultisynchronizeExamples {

    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(10);

    private static final String[] STRINGS = {"Str1", "Str2", "Str3", "Str4"};

    public void multiThread() throws IOException {
        final ThreadLocalRandom random = ThreadLocalRandom.current();
        for (int i = 0, length = 100; i < length; i++) {

            final String firstArg = STRINGS[random.nextInt(0, STRINGS.length)];
            final String secondArg = STRINGS[random.nextInt(0, STRINGS.length)];

            EXECUTOR.execute(() -> {

                String toFSync = firstArg.compareTo(secondArg) > 0 ? firstArg : secondArg;
                String toSSync = toFSync == firstArg ? secondArg : firstArg;

                synchronized (toFSync) {
                    synchronized (toSSync) {
                        System.out.println(firstArg + "_" + secondArg);
                    }
                }
            });
        }
    }
}
