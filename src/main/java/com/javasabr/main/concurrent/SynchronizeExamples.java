package com.javasabr.main.concurrent;

import com.ss.rlib.geom.Vector3f;
import com.ss.rlib.util.pools.Pool;
import com.ss.rlib.util.pools.PoolFactory;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ronn on 26.06.16.
 */
public class SynchronizeExamples {

    private static final ThreadLocal<Random> LOCAL_RANDOM = ThreadLocal.withInitial(Random::new);

    private static final Pool<Vector3f> VECTOR_POOL = PoolFactory.newPool(Vector3f.class);

    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(10);

    public void multiThread() throws IOException {
        for (int i = 0, length = 100; i < length; i++) {
            EXECUTOR.execute(() -> {

                final Random random = LOCAL_RANDOM.get();

                Vector3f first, second, third;

                synchronized (VECTOR_POOL) {
                    first = VECTOR_POOL.take(Vector3f::newInstance);
                    second = VECTOR_POOL.take(Vector3f::newInstance);
                    third = VECTOR_POOL.take(Vector3f::newInstance);
                }

                first.set(random.nextInt(), random.nextInt(), random.nextInt());
                second.set(random.nextInt(), random.nextInt(), random.nextInt());
                third.set(random.nextInt(), random.nextInt(), random.nextInt());

                first.crossLocal(second).multLocal(third);

                System.out.println(first);

                synchronized (VECTOR_POOL) {
                    VECTOR_POOL.put(first);
                    VECTOR_POOL.put(second);
                    VECTOR_POOL.put(third);
                }
            });
        }
    }
}
