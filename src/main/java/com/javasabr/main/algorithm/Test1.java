package com.javasabr.main.algorithm;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10)
@Fork(value = 1)
@Measurement(iterations = 5)
public class Test1 {

    public static int[] ARRAY;

    static {

        final ThreadLocalRandom current = ThreadLocalRandom.current();

        ARRAY = new int[current.nextInt(3, 20)];

        for (int i = 0; i < ARRAY.length; i++) {

            ARRAY[i] = current.nextInt(0, 10000);

            if(current.nextBoolean()) {
                ARRAY[i] *= -1;
            }
        }

        ARRAY = new int[]{3, 1, -5, 3, 3, -5, 0, 10, 1, 1};
    }

    public static void main(String[] main) {
        new Test1().run1();
    }

    @Benchmark
    public int run1() {

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for (int i : ARRAY) {

            if(i < min) {
                min = i;
            }

            if(i > max) {
                max = i;
            }
        }

        final int maxDiff = max - min;
        final int offset = Math.abs(min);

        int[] missed = new int[maxDiff + 1];

        for (int i : ARRAY) {
            final int index = i + offset;
            missed[index] = missed[index] + 1;
        }

        System.out.println("input: " + Arrays.toString(ARRAY));
        System.out.println("min: " + min);
        System.out.println("max: " + max);
        System.out.println("Missing Numbers:");

        for (int i = 0; i < missed.length; i++) {
            if(missed[i] == 0) {
                System.out.print((i + offset) + ", ");
            }
        }

        System.out.println();
        System.out.println("Duplicate:");

        for (int i = 0; i < missed.length; i++) {
            if(missed[i] > 1) {
                System.out.print((i - offset) + " appears " + missed[i] + " times, ");
            }
        }

        return 0;
    }

    @Benchmark
    public int run2() {

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for (int i : ARRAY) {

            if(i < min) {
                min = i;
            }

            if(i > max) {
                max = i;
            }
        }


        return 0;
    }
}
