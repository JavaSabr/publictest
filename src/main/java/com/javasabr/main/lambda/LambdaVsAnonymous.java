package com.javasabr.main.lambda;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OperationsPerInvocation;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10)
@Fork(value = 1)
@Measurement(iterations = 5)
public class LambdaVsAnonymous {

    public static final int OPERATIONS_PER_INVOCATION = 1;

    public static final Consumer<Integer> CONSUMER = new Consumer<Integer>() {

        @Override
        public void accept(final Integer integer) {
            tempValue = integer * 2;
        }
    };

    public static int tempValue;

    private int field1 = 2;
    private int field2 = 2;
    private int field3 = 2;

    @Benchmark
    @OperationsPerInvocation(OPERATIONS_PER_INVOCATION)
    public Consumer<Integer> baseline() {
        return CONSUMER;
    }

    @Benchmark
    @OperationsPerInvocation(OPERATIONS_PER_INVOCATION)
    public Consumer<Integer> anonymous_without_capture() {
        return new Consumer<Integer>() {

            @Override
            public void accept(Integer integer) {
                tempValue = integer * 2;
            }
        };
    }

    @Benchmark
    @OperationsPerInvocation(OPERATIONS_PER_INVOCATION)
    public Consumer<Integer> anonymous_capture_local() {
        int local1 = 2;
        int local2 = 2;
        int local3 = 2;
        return new Consumer<Integer>() {

            @Override
            public void accept(Integer integer) {
                tempValue = integer * local1 * local2 * local3;
            }
        };
    }

    @Benchmark
    @OperationsPerInvocation(OPERATIONS_PER_INVOCATION)
    public Consumer<Integer> anonymous_capture_field() {
        return new Consumer<Integer>() {

            @Override
            public void accept(Integer integer) {
                tempValue = integer * field1 * field2 * field3;
            }
        };
    }

    @Benchmark
    @OperationsPerInvocation(OPERATIONS_PER_INVOCATION)
    public Consumer<Integer> lambda_without_capture() {
        return integer -> tempValue = integer * 2;
    }

    @Benchmark
    @OperationsPerInvocation(OPERATIONS_PER_INVOCATION)
    public Consumer<Integer> lambda_capture_local() {
        int local1 = 2;
        int local2 = 2;
        int local3 = 2;
        return integer -> tempValue = integer * local1 * local2 * local3;
    }

    @Benchmark
    @OperationsPerInvocation(OPERATIONS_PER_INVOCATION)
    public Consumer<Integer> lambda_capture_field() {
        return integer -> tempValue = integer * field1 * field2 * field3;
    }
}
