package com.javasabr.main.time;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;

import static java.time.LocalDateTime.parse;
import static java.time.format.DateTimeFormatter.ofPattern;

/**
 * Created by ronn on 13.09.16.
 */
public class Examples {

    public static void main(final String[] args) throws SQLException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        foo3();
    }

    public static void foo1() {

        final LocalDateTime ldt = LocalDateTime.now();
        final Instant instant = Instant.now();

       // System.out.println("is ldt after than instant? " + ldt.isAfter(instant));
    }

    public static void foo2() {

        final String str = "7/1/2015 11:36 AM";
        final DateTimeFormatter formatter = ofPattern("MM/dd/yyyy hh:mm:ss a");
        final LocalDateTime dateTime = parse(str, formatter);

        System.out.println("1 " + dateTime);
    }

    public static void foo3() {

        final LocalDate now = LocalDate.now();
        final LocalDate before = LocalDate.now().minusDays(1);

        final Period period = Period.between(before, now);
        final Duration duration = Duration.between(before, now);

        System.out.println("period " + period + ", " + duration);
    }
}
